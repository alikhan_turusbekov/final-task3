package kz.aitu.advancedJava.startingPoint.controller;

import kz.aitu.advancedJava.startingPoint.model.Auth;
import kz.aitu.advancedJava.startingPoint.model.Order;
import kz.aitu.advancedJava.startingPoint.model.OrderItem;
import kz.aitu.advancedJava.startingPoint.services.OrderItemService;
import kz.aitu.advancedJava.startingPoint.services.OrderService;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {
    private final OrderService orderService;
    private final OrderItemService orderItemService;

    public OrderController(OrderService orderService, OrderItemService orderItemService) {
        this.orderService = orderService;
        this.orderItemService = orderItemService;
    }

    @GetMapping("/api/orders")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(orderService.findAll());
    }

    @GetMapping("/api/orders/status/{status}")
    public ResponseEntity<?> getProductsByCategoryId(@PathVariable String status) {
        return ResponseEntity.ok(orderService.findAllByStatusId(status));
    }

    @PutMapping("/api/orders/status/update/{orderId}")
    public ResponseEntity<?> updateStatusByOrderId(@PathVariable Long orderId) {
        return ResponseEntity.ok(orderService.updateStatusByOrderId(orderId));
    }

    @PostMapping("/create-order")
    public void createOrder(@RequestHeader("auth") String token, @RequestBody List<OrderItem> orderItemList) {
        orderItemService.createOrderItems(orderService.createOrder(token, orderItemList), orderItemList);
    }

    @GetMapping("/get-my-orders")
    public ResponseEntity<?> getOrders(@RequestHeader("Auth") String token){
        return ResponseEntity.ok(orderService.getMyOrders(token));
    }

}