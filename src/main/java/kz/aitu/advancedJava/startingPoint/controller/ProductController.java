package kz.aitu.advancedJava.startingPoint.controller;

import kz.aitu.advancedJava.startingPoint.services.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/api/products")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(productService.findAll());
    }

    @GetMapping("/api/products/{id}")
    public void findById(@PathVariable long id) {
        productService.findById(id);
    }

    @GetMapping("/api/products/category/{categoryId}")
    public ResponseEntity<?> getProductsByCategoryId(@PathVariable Long categoryId) {
        return ResponseEntity.ok(productService.findAllByCategoryId(categoryId));
    }
}
