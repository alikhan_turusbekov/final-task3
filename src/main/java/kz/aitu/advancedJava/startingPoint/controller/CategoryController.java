package kz.aitu.advancedJava.startingPoint.controller;

import kz.aitu.advancedJava.startingPoint.services.CategoryService;
import kz.aitu.advancedJava.startingPoint.services.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/api/categories")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(categoryService.findAll());
    }

    @GetMapping("/api/categories/{id}")
    public void deleteById(@PathVariable long id) {
        categoryService.findById(id);
    }

}
