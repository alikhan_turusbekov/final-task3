package kz.aitu.advancedJava.startingPoint.controller;

import kz.aitu.advancedJava.startingPoint.model.Auth;
import kz.aitu.advancedJava.startingPoint.services.AuthService;
import kz.aitu.advancedJava.startingPoint.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    private final AuthService authService;

    @GetMapping("/customers/me")
    public ResponseEntity<?> getMe(@RequestHeader("Auth") String token) throws Exception {
        Auth auth = authService.getAuthByToken(token);
        return ResponseEntity.ok(customerService.getCustomerById(auth.getCustomerId()).orElseThrow(ChangeSetPersister.NotFoundException::new));
    }

    @GetMapping("/customers/all/value")
    public ResponseEntity<?> getCustomersValue() {
        return ResponseEntity.ok(customerService.getCustomersValue());
    }
}
