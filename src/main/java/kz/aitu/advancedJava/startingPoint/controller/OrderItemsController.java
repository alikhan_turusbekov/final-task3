package kz.aitu.advancedJava.startingPoint.controller;

import kz.aitu.advancedJava.startingPoint.model.OrderItem;
import kz.aitu.advancedJava.startingPoint.services.OrderItemService;
import kz.aitu.advancedJava.startingPoint.services.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class OrderItemsController {

    private final OrderItemService orderItemService;

    public OrderItemsController(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @GetMapping("/get-my-orderitems/{orderId}")
    public ResponseEntity<?> getOrderItemsByOrderId(@PathVariable Long orderId) {
        return ResponseEntity.ok(orderItemService.findAllByOrderId(orderId));
    }

    @GetMapping("/api/orders/get-dashboard")
    public ResponseEntity<?> createDashboard() {
        return ResponseEntity.ok(orderItemService.createDashboard());
    }

}

