package kz.aitu.advancedJava.startingPoint.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "auth")
public class Auth {
    @Id
    private long id;
    private String login;
    @JsonIgnore
    private String password;
    private long customerId;
    private String token;

    public long getCustomerId() {
        return customerId;
    }

    public String getLogin() {
        return login;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }
}
