package kz.aitu.advancedJava.startingPoint.services;

import kz.aitu.advancedJava.startingPoint.model.*;
import kz.aitu.advancedJava.startingPoint.repository.CustomerRepository;
import kz.aitu.advancedJava.startingPoint.repository.OrderItemRepository;
import kz.aitu.advancedJava.startingPoint.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final OrderRepository orderRepository;

    public CustomerService(CustomerRepository customerRepository, OrderRepository orderRepository) {
        this.customerRepository = customerRepository;
        this.orderRepository = orderRepository;
    }

    public Iterable<Customer> getAll(){
        return customerRepository.findAll();
    }

    public Optional<Customer> getCustomerById(Long customerId) {
        return customerRepository.findById(customerId);
    }

    public Iterable<Customer> getCustomersValue() {
        List<Customer> customers = (List<Customer>) customerRepository.findAll();
        List<Order> orders = (List<Order>) orderRepository.findAll();
        int n = customers.size();
        int n2 = orders.size();
        for (int i=0; i<n; i++){
            customers.get(i).setPassword("");
            int sum = 0;
            for (int j=0; j<n2; j++){
                if(customers.get(i).getId() == orders.get(j).getCustomerId()){
                    sum += orders.get(j).getTotalPrice();
                }
            }
            customers.get(i).setPassword(Integer.toString(sum));
            if (sum == 0){
                customers.remove(i);
            }
        };
        return customers;
    }
}
