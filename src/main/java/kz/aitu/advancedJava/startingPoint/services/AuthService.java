package kz.aitu.advancedJava.startingPoint.services;

import kz.aitu.advancedJava.startingPoint.model.Auth;
import kz.aitu.advancedJava.startingPoint.repository.AuthRepository;
import kz.aitu.advancedJava.startingPoint.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService  {
    private final AuthRepository authRepository;
    private final CustomerRepository customerRepository;

    @Transactional
    public Auth login(Auth auth) throws Exception {
        Auth authDB = authRepository.findByLoginAndPassword(auth.getLogin(), auth.getPassword());

        if (authDB==null){
            throw new Exception();
        }

        String token = UUID.randomUUID().toString();

        authDB.setToken(token);
         return authRepository.save(authDB);

    }

    public Auth getAuthByToken(String token) {
        return authRepository.findByToken(token);
    }

    public Auth loginAdmin(Auth auth) throws Exception{
        Auth authDB = authRepository.findByLoginAndPassword(auth.getLogin(), auth.getPassword());

        if (authDB==null||!((auth.getLogin().equals("admin@admin.com"))&&(auth.getPassword().equals("admin")))){
            throw new Exception();
        }

        String token = UUID.randomUUID().toString();

        authDB.setToken(token);
        return authRepository.save(authDB);
    }
}
