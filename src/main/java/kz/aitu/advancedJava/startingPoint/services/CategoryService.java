package kz.aitu.advancedJava.startingPoint.services;

import kz.aitu.advancedJava.startingPoint.model.Category;
import kz.aitu.advancedJava.startingPoint.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Iterable<Category> findAll(){
        return categoryRepository.findAll();
    }

    public Optional<Category> findById(long id) { return  categoryRepository.findById(id); }

}
