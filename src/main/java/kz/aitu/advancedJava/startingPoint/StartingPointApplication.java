package kz.aitu.advancedJava.startingPoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartingPointApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartingPointApplication.class, args);
	}

}
