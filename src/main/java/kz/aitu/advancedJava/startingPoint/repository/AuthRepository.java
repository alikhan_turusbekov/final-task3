package kz.aitu.advancedJava.startingPoint.repository;

import kz.aitu.advancedJava.startingPoint.model.Auth;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthRepository extends CrudRepository< Auth, Long> {
    Auth findByLoginAndPassword(String login, String password);
    Auth findByToken(String token);
}
