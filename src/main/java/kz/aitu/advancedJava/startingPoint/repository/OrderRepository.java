package kz.aitu.advancedJava.startingPoint.repository;

import kz.aitu.advancedJava.startingPoint.model.Order;
import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
    @Transactional
    @Modifying
    @Query(value = "insert into customers_orders (customer_id,date,total_price, status) values(:customer_id, :date, :total_price, :status)", nativeQuery = true )
    void insertOrder(@Param("customer_id") long customer_id, @Param("date") LocalDate date, @Param("total_price") int total_price, @Param("status") String status);

    @Query(value = "SELECT * from customers_orders where status=:status", nativeQuery = true )
    List<Order> getAllByStatus(@Param("status") String status);
}
