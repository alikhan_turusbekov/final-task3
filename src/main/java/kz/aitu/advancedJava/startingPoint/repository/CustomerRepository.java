package kz.aitu.advancedJava.startingPoint.repository;

import kz.aitu.advancedJava.startingPoint.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long > {
}
