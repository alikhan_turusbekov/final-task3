var app = angular.module('aitu-project', []);

app.controller('AdminCtrl', function($scope, $http) {
    $scope.auth = {
        login: '',
        password: ''
    };

    $scope.login = function(auth) {
        $http({
            url: 'http://127.0.0.1:8081/login/admin',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            data: auth
        })
            .then(function (response) {
                    $scope.auth = response.data;
                    $scope.valueCustomers();
                    $scope.getOrders();
                    $scope.createDashboard();
                },
                function (response) { // optional
                    $scope.auth = {}
                });
    };

    $scope.ordersList = [];

    $scope.getOrders = function() {
        $http({
            url: 'http://127.0.0.1:8081/api/orders',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.ordersList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getOrdersByStatus = function(a) {
        $http({
            url: 'http://127.0.0.1:8081/api/orders/status/'+a,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.ordersList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.update = function(order_id) {
        $http({
            url: 'http://127.0.0.1:8081/api/orders/status/update/'+order_id,
            method: "PUT",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
        })
            .then(function (response) {
                    console.log(response);
                    $scope.getOrders();
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.dashboard = [];

    $scope.createDashboard = function () {
        $http({
            url: 'http://127.0.0.1:8081/api/orders/get-dashboard',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.dashboard = response.data;
        }, function (response){
            console.log(response);
        })
    };

    $scope.customers = [];

    $scope.valueCustomers = function () {
        $http({
            url: 'http://127.0.0.1:8081/customers/all/value',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.customers = response.data;
        }, function (response){
            console.log(response);
        })
    };
});


